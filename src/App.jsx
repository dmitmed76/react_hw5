import { useState, useEffect } from 'react'
import { Routes, Route } from 'react-router-dom';
import { useDispatch, useSelector } from 'react-redux';

import { AppWrapper, ButtonModalWrapper, ButtonModal, MenuWrapper, LinkCustom } from './styledApp';
import { Modal } from './components/Modal';
import { Header } from './components/Header';
import { Home } from './pages/Home';
import { Favorites } from './pages/Favorites';
import { Basket } from './pages/Basket';
import { getGoods } from './features/goods/goodsSlice';
import { actionIsModal } from './features/modal/isModalSlice';
import { actionIsModalTwo } from './features/modal/isModalTwoSlice';

function App() {
	const dispatch = useDispatch();

	const projectors = useSelector((state) => state.goods.projectors);
	const { status, error } = useSelector((state) => state.goods);
	const isModal = useSelector((state) => state.isModal.isModal);
	const isModalTwo = useSelector((state) => state.isModalTwo.isModalTwo);
	const favorites = useSelector((state) => state.isFavorite.favorites);

	const [basket, setBasket] = useState(JSON.parse(localStorage.getItem('basket')) || []);
	const [currentGoods, setCurrentGoods] = useState({});

	localStorage.setItem('basket', JSON.stringify(basket));
	localStorage.setItem('favorites', JSON.stringify(favorites));

	useEffect(() => {
		dispatch(getGoods());
	}, [dispatch]);

	const hendlerModal = () => {
		dispatch(actionIsModal(!isModal))
	}

	const hendlerModalTwo = () => {
		dispatch(actionIsModalTwo(!isModalTwo))
	}

	const hendlerCurrentGoods = (currentGoods) => {
		setCurrentGoods({ ...currentGoods });
	}

	const hendlerBasket = (currentGoods) => {
		let isInArray = false;
		basket.forEach(el => {
			if (el.article === currentGoods.article)
				isInArray = true;
		});
		if (!isInArray)
			setBasket([...basket, currentGoods]);
		hendlerModal()
	}

	const deleteOrder = (currentGoods) => {
		setBasket(basket.filter(el => el.article !== currentGoods.article))
		hendlerModalTwo()
	}

	const actions = (
		<ButtonModalWrapper>
			<ButtonModal type='button'
				onClick={isModal ? () => hendlerBasket(currentGoods) : () => deleteOrder({ ...currentGoods })}>ok</ButtonModal>
			<ButtonModal type='button' backgroundColor={'#B43727'}
				onClick={isModal ? hendlerModal : hendlerModalTwo}>cancel</ButtonModal>
		</ButtonModalWrapper>
	)

	return (
		<AppWrapper>
			<Header
				countBasket={basket.length}
				countFavorite={favorites.length} />

			{status === 'loading' && <h2>Loading...</h2>}
			{error && <h2>An error occured: {error}</h2>}

			<MenuWrapper>
				<LinkCustom to="/">Home</LinkCustom>
				<LinkCustom to="/Favorites">Favorites</LinkCustom>
				<LinkCustom to="/Basket">Basket</LinkCustom>
			</MenuWrapper>

			<Routes>
				<Route path='/' element={<Home
					hendlerCurrentGoods={hendlerCurrentGoods}
				/>} />
				<Route path='/Favorites' element={<Favorites
					favorites={projectors.filter((el) => favorites.includes(el.article))}
				/>}
				/>
				<Route path='/Basket' element={<Basket
					basket={basket}
					deleteOrder={deleteOrder}
					hendlerCurrentGoods={hendlerCurrentGoods}
				/>}
				/>
			</Routes>

			{isModal && <Modal
				actions={actions}
				closeButton={hendlerModal}
				header='Ви дійсно хочете додати цей товар в корзину?'
				text="Натисніть 'ОК' для відправлення товару в корзину або натисніть 'CANCEL' для відміни операції "
				backgroundColor='lightblue'
			/>}

			{isModalTwo && <Modal
				actions={actions}
				closeButton={hendlerModalTwo}
				header='Ви дійсно хочете видалити цей товар з корзини?'
				text="Натисніть 'ОК' для іидалення товару з корзини або натисніть 'CANCEL' для відміни операції"
			/>}

		</AppWrapper >

	)
}

export default App;

