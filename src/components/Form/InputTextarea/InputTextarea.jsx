import PropTypes from 'prop-types';

import { LableStyled, LableTitleStyled, StyledField, ErrorMessegeStyled } from './styledInputTextarea'

export function InputTextarea({ lable, error, nameTextArea, rows, cols, placeholder, ...restProps}) {

  return (
    <LableStyled color={error && 'error'}>
        <LableTitleStyled>{lable}</LableTitleStyled>
        <StyledField forwardedAs="textarea" border={error && 'error'} name={nameTextArea} rows={rows} cols={cols} placeholder={placeholder} {...restProps}/>
        <ErrorMessegeStyled name={nameTextArea} component='p'/>
    </LableStyled>
  )
}

InputTextarea.propTypes = {
    lable: PropTypes.string,
    error: PropTypes.object,
    nameTextArea: PropTypes.string,
    rows: PropTypes.number,
    cols: PropTypes.number,
    placeholder: PropTypes.string,
}
