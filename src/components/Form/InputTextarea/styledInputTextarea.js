import styled from 'styled-components';
import { Field, ErrorMessage } from 'formik';

const LableStyled = styled.label`
font-size: 24px;
text-align: center; 
color: ${(props) => props.color === 'error' ? '#dc3545' : 'darkGreen'};
`
const LableTitleStyled = styled.p`
margin: 20px 0 5px 0;
`
const StyledField = styled(Field)`
resize: none;
outline:none;
border: ${(props) => props.border === 'error' ? '1px solid #dc3545' : '1px solid darkGreen'};
border-radius: 5px;
font-size: 22px;
text-align: center; 
margin: 0 50px;
&:hover,
&:focus{
border: none;
box-shadow: 0 0 0 0.25rem rgb(0 100 0 / 35%);
};
`

const ErrorMessegeStyled = styled(ErrorMessage)`
margin-top: 4px;
text-align: center;
color: #dc3545;
`

export { LableStyled, LableTitleStyled, StyledField, ErrorMessegeStyled };