
import { Input } from './Input';
import { InputTextarea } from './InputTextarea';
import { InputPhone } from './InputPhone';

export { Input, InputTextarea, InputPhone };

