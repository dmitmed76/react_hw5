import PropTypes from 'prop-types';
import { useSelector } from 'react-redux';

import { CardsWrapper, CardWrapper } from './styledGoodsCards'
import { Card } from './components/Card';


function GoodsCards({ hendlerCurrentGoods }) {

	const projectors = useSelector((state) => state.goods.projectors);
	const favorites = useSelector((state) => state.isFavorite.favorites);
	return (
		<CardsWrapper>
			{projectors.map((el, index) => (
				<CardWrapper key={index}>
					<Card
						isFavorite={favorites.includes(el.article)}
						item={el}
						hendlerCurrentGoods={hendlerCurrentGoods}
					/>
				</CardWrapper>
			))}

		</CardsWrapper>

	);

}

GoodsCards.propTypes = {
	hendlerCurrentGoods: PropTypes.func,
}

export { GoodsCards };

