import { createSlice } from "@reduxjs/toolkit";

const initialState = {
	favorites: (JSON.parse(localStorage.getItem('favorites')) || []),
}

export const isFavoriteSlice = createSlice({
	name: 'isFavorite',
	initialState,
	reducers: {
		actionAddIsFavorite: (state, action) => {
			state.favorites = [...state.favorites, action.payload]
		},
		actionRemoveIsFavorites: (state, action) => {
			state.favorites = state.favorites.filter(el => el !== action.payload.article)
		},
	}
})

export const { actionAddIsFavorite, actionRemoveIsFavorites } = isFavoriteSlice.actions
export default isFavoriteSlice.reducer