import PropTypes from 'prop-types';
import { useDispatch, useSelector } from 'react-redux';

import { actionIsModalTwo } from '../../features/modal/isModalTwoSlice';
import { Sidebar } from './components/Sidebar';


import {
	BasketWrapper,
	BasketCardTitle,
	BasketGoodsColor,
	BasketCardArt,
	BasketCardImg,
	BasketCardPrice,
	BasketCardsWrapper,
	DeleteButton,
	Icons
} from './styledBasket'

export default function Basket({ basket, hendlerCurrentGoods }) {

	const dispatch = useDispatch();

	const isModalTwo = useSelector((state) => state.isModalTwo.isModalTwo);


	const hendlerModalTwo = () => {
		dispatch(actionIsModalTwo(!isModalTwo))
	}

	return (
		<>
			<BasketWrapper>
				<div>
					{basket.map((el) => (
						<BasketCardsWrapper key={el.article} >
							<BasketCardImg src={el.url} alt={el.title} />
							<BasketCardTitle>{el.title}</BasketCardTitle>
							<BasketGoodsColor>Колір: {el.color}</BasketGoodsColor>
							<BasketCardArt>Артикуль: {el.article}</BasketCardArt>
							<BasketCardPrice>Ціна: {el.price} грн.</BasketCardPrice>
							<DeleteButton type="button" onClick={() => {
								hendlerModalTwo()
								hendlerCurrentGoods(el)
							}} >
								<Icons />
							</DeleteButton>
						</BasketCardsWrapper>
					)
					)}
				</div>

				<div>
					<Sidebar basket={basket}/>
				</div>
			</BasketWrapper>
		</>


	)
}

Basket.propTypes = {
	basket: PropTypes.array,
	hendlerCurrentGoods: PropTypes.func
}