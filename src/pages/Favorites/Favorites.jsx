import PropTypes from 'prop-types';

import {
	FavoritesCardTitle,
	FavoritesGoodsColor,
	FavoritesCardArt,
	FavoritesCardImg,
	FavoritesCardPrice,
	FavoritesCardsWrapper
} from './styledFavorites'

export default function Favorites({ favorites }) {
	
	return (
		<>
			{favorites.map((el, index) => (
				<FavoritesCardsWrapper key={index}>
					<FavoritesCardImg src={el.url} alt={el.title} />
					<FavoritesCardTitle>{el.title}</FavoritesCardTitle>
					<FavoritesGoodsColor>Колір: {el.color}</FavoritesGoodsColor>
					<FavoritesCardArt>Артикуль: {el.article}</FavoritesCardArt>
					<FavoritesCardPrice>Ціна: {el.price} грн.</FavoritesCardPrice>
				</FavoritesCardsWrapper>
			)
			)}
		</>
	)
}

Favorites.propTypes = {
	favorites: PropTypes.array,
}